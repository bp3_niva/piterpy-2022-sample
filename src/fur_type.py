class FurType:
    """Базовый класс типа шёрстки."""

    name: str
    dirty: bool = False

    def get_dirty(self):
        """Испачкать шёрстку."""
        self.dirty = True

    def get_bath(self):
        """Помыть шёрстку."""
        self.dirty = False

    def get_name(self) -> str:
        res = self.name
        if self.dirty:
            res += " испачканый"
        return res


class LongFur(FurType):
    name = "Длинношёрстный"


class ShortFur(FurType):
    name = "Гладкошёрстный"