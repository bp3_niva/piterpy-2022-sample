from src.fur_color import FurColor
from src.fur_type import FurType


class Cat:
    """Класс котика."""

    color: FurColor
    fur_type: FurType

    def __init__(self, color: FurColor, fur_type: FurType):
        self.color = color
        self.fur_type = fur_type

    def __str__(self):
        return f"{self.fur_type.get_name()} {self.color.get_name()} котик."

    def __repr__(self):
        return str(self)

    def take_a_walk(self):
        """Прогуляться."""
        self.fur_type.get_dirty()

    def take_a_bath(self):
        """Искупаться."""
        self.fur_type.get_bath()
