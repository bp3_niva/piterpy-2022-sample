class FurColor:
    """Базовый класс цвета шёрстки."""

    name: str

    def get_name(self) -> str:
        return self.name


class GingerColor(FurColor):
    name = "рыжий"


class BlackColor(FurColor):
    name = "чёрный"


class WhiteColor(FurColor):
    name = "белый"